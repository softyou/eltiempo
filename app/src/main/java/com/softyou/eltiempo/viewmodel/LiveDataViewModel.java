package com.softyou.eltiempo.viewmodel;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.softyou.eltiempo.data.model.Item;
import com.softyou.eltiempo.domain.ApoloUseCase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

public class LiveDataViewModel extends ViewModel {

    ApoloUseCase apoloUseCase;

    @Inject
    public LiveDataViewModel(ApoloUseCase apoloUseCase){
        this.apoloUseCase = apoloUseCase;
    }

    public MutableLiveData<List<InfoCard>> liveListApolo = new MutableLiveData<List<InfoCard>>();

    public MutableLiveData<InfoCard> liveApollo = new MutableLiveData<>();

    @SuppressLint("CheckResult")
    public void getApoloList(){
        Log.i("Api: ", "Se hace la peticiòn");
        apoloUseCase.getApoloUseCase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item->{
                    liveListApolo.setValue(item);
                }, e->{
                    Log.println(Log.ERROR, "Error", e.getMessage());
                });
    }

    @SuppressLint("CheckResult")
    public void getObtenerListfilter(String filtro){
        Log.i("Api: ", "Se hace la peticiòn");
        apoloUseCase.getApoloFilterUseCase(filtro)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item->{
                    liveListApolo.setValue(item);
                }, e->{
                    Log.println(Log.ERROR, "Error", e.getMessage());
                });
    }

    @SuppressLint("CheckResult")
    public void getItemDBApollo(int id){
        apoloUseCase.getItemDBApollo(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item->{
                    liveApollo.setValue(item);
                }, e->{
                    Log.println(Log.ERROR, "Error", e.getMessage());
                });
    }

    @SuppressLint("CheckResult")
    public void updateItemDBApollo(int id,boolean fav){
        apoloUseCase.updateItemDBApollo(id, fav);
    }
}
