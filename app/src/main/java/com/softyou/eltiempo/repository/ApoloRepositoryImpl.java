package com.softyou.eltiempo.repository;

import android.icu.text.IDNA;

import com.softyou.eltiempo.data.ApoloInfoDBRepository;
import com.softyou.eltiempo.data.ApoloInfoRepository;
import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.softyou.eltiempo.data.model.Apolo;
import com.softyou.eltiempo.domain.IApoloRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public class ApoloRepositoryImpl implements IApoloRepository {

    ApoloInfoRepository apoloInfoRepository;
    ApoloInfoDBRepository apoloInfoDBRepository;

    @Inject
    public ApoloRepositoryImpl(ApoloInfoRepository apoloInfoRepository,
                               ApoloInfoDBRepository apoloInfoDBRepository){
        this.apoloInfoRepository = apoloInfoRepository;
        this.apoloInfoDBRepository = apoloInfoDBRepository;
    }

    @Override
    public Observable<Apolo> getApoloRepository() {
        return apoloInfoRepository.getApoloInfoApi();
    }

    @Override
    public Flowable<List<InfoCard>> getApoloFilterRepository(String filtro) {
        return apoloInfoDBRepository.getApoloDBFilter(filtro);
    }

    @Override
    public int getRowCount() {
        return apoloInfoDBRepository.getCountRows();
    }

    public Flowable<List<InfoCard>> getAll(){
        return apoloInfoDBRepository.getAll();
    }

    @Override
    public void insertInfoDB(List<InfoCard> info) {
        apoloInfoDBRepository.insertInfoDB(info);
    }

    @Override
    public Flowable<InfoCard> getItemDBApollo(int id) {
        return apoloInfoDBRepository.getItemApollo(id);
    }

    @Override
    public void updateInfoCardDB(int id, boolean fav) {
        apoloInfoDBRepository.updateInfoCard(id, fav);
    }
}
