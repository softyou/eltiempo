package com.softyou.eltiempo.data.model;

import java.util.List;

public class Item {

    private String href;
    private List<Data> data;
    private List<Link> links;

    public Item(String href, List<Data> data, List<Link> links) {
        this.href = href;
        this.data = data;
        this.links = links;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
