package com.softyou.eltiempo.data.model;

public class Metadata {

    private Integer total_hits;

    public Metadata(Integer total_hits) {
        this.total_hits = total_hits;
    }

    public Integer getTotal_hits() {
        return total_hits;
    }

    public void setTotal_hits(Integer total_hits) {
        this.total_hits = total_hits;
    }
}
