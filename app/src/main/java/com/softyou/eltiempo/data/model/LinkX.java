package com.softyou.eltiempo.data.model;

public class LinkX {

    private String href;
    private String prompt;
    private String rel;

    public LinkX(String href, String prompt, String rel) {
        this.href = href;
        this.prompt = prompt;
        this.rel = rel;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }
}
