package com.softyou.eltiempo.data;

import android.content.Context;

import com.softyou.eltiempo.data.databaseRoom.ApoloDao;
import com.softyou.eltiempo.data.databaseRoom.InfoCard;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public class ApoloInfoDBRepository {

    ApoloDao apoloDao;

    @Inject
    public ApoloInfoDBRepository(
            ApoloDao apoloDao
    ) {
        this.apoloDao = apoloDao;
    }

    public Flowable<List<InfoCard>> getAll(){
        return apoloDao.getAll();
    }

    public Flowable<List<InfoCard>> getApoloDBFilter(String filtro){
        return apoloDao.getQuerySearch(filtro);
    }

    public int getCountRows(){
        return apoloDao.getRowCount();
    }

    public void insertInfoDB(List<InfoCard> info){
        apoloDao.insertAll(info);
    }

    public Flowable<InfoCard> getItemApollo(int id){
        return apoloDao.getItemApollo(id);
    }

    public void updateInfoCard(int id, boolean fav){
        apoloDao.updateItemApollo(id, fav);
    }
}
