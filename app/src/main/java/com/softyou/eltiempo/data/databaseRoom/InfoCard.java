package com.softyou.eltiempo.data.databaseRoom;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "apolo")
public class InfoCard {

    @PrimaryKey @NotNull private Integer Id;

    @ColumnInfo(name = "tittle") private String tittle;

    @ColumnInfo(name = "hrefImg") private String hrefImg;

    @ColumnInfo(name = "fav") private boolean fav;

    public InfoCard(String tittle, String hrefImg) {
        this.tittle = tittle;
        this.hrefImg = hrefImg;
        this.fav = false;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(@NotNull Integer id) {
        Id = id;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getHrefImg() {
        return hrefImg;
    }

    public void setHrefImg(String hrefImg) {
        this.hrefImg = hrefImg;
    }

    public boolean isFav() {
        return fav;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }
}
