package com.softyou.eltiempo.data;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.room.Room;

import com.softyou.eltiempo.data.databaseRoom.ApoloDao;
import com.softyou.eltiempo.data.databaseRoom.DataBaseRoom;
import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.softyou.eltiempo.data.model.Apolo;
import com.softyou.eltiempo.data.model.Item;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ActivityContext;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApoloInfoRepository {

    IApiService iApiService;

    @Inject
    public ApoloInfoRepository(
            IApiService service
    ) {
        this.iApiService = service;
    }

    @SuppressLint("CheckResult")
    public Observable<Apolo> getApoloInfoApi(){
        return iApiService.getInfoApolo();
    }

}
