package com.softyou.eltiempo.data.databaseRoom;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

@Dao
public interface ApoloDao {

    @Query("SELECT * FROM apolo")
    Flowable<List<InfoCard>> getAll();

    @Query("SELECT * FROM apolo where apolo.tittle like '%' || :palabra || '%' ")
    Flowable<List<InfoCard>> getQuerySearch(String palabra);

    @Query("SELECT COUNT(id) FROM apolo")
    int getRowCount();

    @Query("SELECT * FROM apolo where apolo.id = :id")
    Flowable<InfoCard> getItemApollo(int id);

    @Insert
    void insertAll(List<InfoCard> info);

    @Query("update apolo set fav = :fav where id = :id")
    void updateItemApollo(int id, boolean fav);
}
