package com.softyou.eltiempo.data.model;

public class Apolo {

    private Collection collection;

    Apolo(Collection collection){
        this.collection = collection;
    }

    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }
}
