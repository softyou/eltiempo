package com.softyou.eltiempo.data.model;

import java.util.List;

public class Data {

    private List<String> album;
    private String center;
    private String date_created;
    private String description;
    private List<String> keywords;
    private String location;
    private String media_type;
    private String nasa_id;
    private String photographer;
    private String title;

    public Data(List<String> album, String center, String date_created, String description,
                List<String> keywords, String location, String media_type, String nasa_id,
                String photographer, String title) {
        this.album = album;
        this.center = center;
        this.date_created = date_created;
        this.description = description;
        this.keywords = keywords;
        this.location = location;
        this.media_type = media_type;
        this.nasa_id = nasa_id;
        this.photographer = photographer;
        this.title = title;
    }

    public List<String> getAlbum() {
        return album;
    }

    public void setAlbum(List<String> album) {
        this.album = album;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getNasa_id() {
        return nasa_id;
    }

    public void setNasa_id(String nasa_id) {
        this.nasa_id = nasa_id;
    }

    public String getPhotographer() {
        return photographer;
    }

    public void setPhotographer(String photographer) {
        this.photographer = photographer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
