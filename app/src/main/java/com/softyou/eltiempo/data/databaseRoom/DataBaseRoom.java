package com.softyou.eltiempo.data.databaseRoom;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = InfoCard.class, version = 2)
public abstract class DataBaseRoom extends RoomDatabase {
    public abstract ApoloDao userDao();
}
