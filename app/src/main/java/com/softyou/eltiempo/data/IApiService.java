package com.softyou.eltiempo.data;

import com.softyou.eltiempo.data.model.Apolo;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface IApiService {

    @GET("search?q=apollo%2011")
    Observable<Apolo> getInfoApolo();

}
