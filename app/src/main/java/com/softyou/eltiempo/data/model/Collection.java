package com.softyou.eltiempo.data.model;

import java.util.List;

public class Collection {

    private String href;
    private String version;
    private List<Item> items;
    private List<LinkX> links;
    private Metadata metadata;

    public Collection(String href, String version, List<Item> items, List<LinkX> links, Metadata metadata) {
        this.href = href;
        this.version = version;
        this.items = items;
        this.links = links;
        this.metadata = metadata;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<LinkX> getLinks() {
        return links;
    }

    public void setLinks(List<LinkX> links) {
        this.links = links;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
