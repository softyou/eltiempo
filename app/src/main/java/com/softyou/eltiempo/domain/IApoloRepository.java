package com.softyou.eltiempo.domain;

import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.softyou.eltiempo.data.model.Apolo;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public interface IApoloRepository {

    //Api
    Observable<Apolo> getApoloRepository();

    //DB
    int getRowCount();
    Flowable<List<InfoCard>> getApoloFilterRepository(String filtro);
    Flowable<List<InfoCard>> getAll();
    void insertInfoDB(List<InfoCard> info);
    Flowable<InfoCard> getItemDBApollo(int id);
    void updateInfoCardDB(int id, boolean fav);
}
