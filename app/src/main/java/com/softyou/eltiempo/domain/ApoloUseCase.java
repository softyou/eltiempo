package com.softyou.eltiempo.domain;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.util.Log;

import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.softyou.eltiempo.data.model.Apolo;
import com.softyou.eltiempo.data.model.Item;
import com.softyou.eltiempo.repository.ApoloRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApoloUseCase{

    IApoloRepository iApoloRepository;

    @Inject
    public ApoloUseCase(ApoloRepositoryImpl apoloRepositoryImpl){
        this.iApoloRepository = apoloRepositoryImpl;
    }

    @SuppressLint("CheckResult")
    public Flowable<List<InfoCard>> getApoloUseCase() {
        if(iApoloRepository.getRowCount()<=0){
            iApoloRepository.getApoloRepository()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(item->{
                        List<InfoCard> data = new ArrayList<>();
                        for (Item info:item.getCollection().getItems()){
                            if(info.getData()!= null &&  info.getLinks()!= null){
                                data.add(new InfoCard(info.getData().get(0).getTitle(),
                                        info.getLinks().get(0).getHref()));
                                Log.println(Log.ERROR, "Se guardo",info.getData().get(0).getTitle());
                            }
                        }
                        iApoloRepository.insertInfoDB(data);
                    }, e->{
                        Log.println(Log.ERROR, "Error", e.getMessage());
                    });
        }
        return iApoloRepository.getAll();
    }

    public Flowable<List<InfoCard>> getApoloFilterUseCase(String filtro) {
        return iApoloRepository.getApoloFilterRepository(filtro);
    }

    public Flowable<InfoCard> getItemDBApollo(int id){
        return iApoloRepository.getItemDBApollo(id);
    }

    public void updateItemDBApollo(int id, boolean fav){
        iApoloRepository.updateInfoCardDB(id, fav);
    }
}
