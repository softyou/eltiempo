package com.softyou.eltiempo.ui.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.softyou.eltiempo.DetailsApolo;
import com.softyou.eltiempo.R;
import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListApoloAdapter extends RecyclerView.Adapter<ListApoloAdapter.ViewHolder>{

    private List<InfoCard> listInfo;

    public ListApoloAdapter(List<InfoCard> listInfo){
        this.listInfo = listInfo;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_apolo, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTittle().setText(this.listInfo.get(position).getTittle());
        Picasso.with(holder.imgView.getContext())
                .load(listInfo.get(position).getHrefImg())
                .into(holder.imgView);

        if(this.listInfo.get(position).isFav()){
            holder.imgFav.setVisibility(View.VISIBLE);
        }else{
            holder.imgFav.setVisibility(View.INVISIBLE);
        }

        holder.layout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent details = new Intent(view.getContext(), DetailsApolo.class);
                details.putExtra("id",listInfo.get(position).getId());
                details.putExtra("tittle",listInfo.get(position).getTittle());
                details.putExtra("href",listInfo.get(position).getHrefImg());
                details.putExtra("fav",listInfo.get(position).isFav());
                view.getContext().startActivity(details);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.listInfo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tittle;
        private final ImageView imgView;
        private final ConstraintLayout layout;
        private final ImageView imgFav;

        public ViewHolder(View view) {
            super(view);
            tittle = (TextView) view.findViewById(R.id.textTittle);
            imgView = (ImageView) view.findViewById(R.id.imageApolo);
            layout = (ConstraintLayout) view.findViewById(R.id.cardData);
            imgFav = (ImageView) view.findViewById(R.id.imagefav);
        }

        public TextView getTittle() {
            return tittle;
        }

        public ImageView getImageFav() {
            return imgView;
        }

    }
}
