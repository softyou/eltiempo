package com.softyou.eltiempo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.softyou.eltiempo.ui.adapters.ListApoloAdapter;
import com.softyou.eltiempo.viewmodel.LiveDataViewModel;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity
        implements OnQueryTextListener{

    @Inject
    LiveDataViewModel viewModel;

    private List<InfoCard> listInfo;
    private List<InfoCard> listInfoSeach;

    private RecyclerView listApolo;
    private ProgressBar load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SearchView simpleSearchView = (SearchView) findViewById(R.id.searchView);

        listApolo = findViewById(R.id.listApolo);
        load = findViewById(R.id.loadInfo);

        viewModel.getApoloList();

        viewModel.liveListApolo.observe(this, s -> {
            listInfo = s;
            listInfoSeach = s;
            updateListApolo(s);
        });

        listApolo.setLayoutManager(new LinearLayoutManager(this.getApplicationContext(),
                                                            LinearLayoutManager.VERTICAL,
                                                false));

        simpleSearchView.setOnQueryTextListener(new OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String s) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                viewModel.getObtenerListfilter(s);
                return false;
            }

        });


    }

    private void updateListApolo(List<InfoCard> data) {
        setAdapter(data);
        listApolo.setLayoutManager(new LinearLayoutManager(this.getApplicationContext(),
                LinearLayoutManager.VERTICAL,
                false));
    }

    private void setAdapter(List<InfoCard> data) {
        load.setVisibility(View.INVISIBLE);
        if(data != null){
            listApolo.setAdapter(new ListApoloAdapter(data));
            Log.println(Log.INFO, "Proceso ejecutado Bien", "Datos mostrados correctamente");
        }else{
            Toast.makeText(this.getApplicationContext(), "Error en la peticion", Toast.LENGTH_LONG).show();
            Log.println(Log.ERROR, "Error la busqueda", "Hay:" + data.size());
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listInfo.clear();
        if (charText.length() == 0) {
            listInfo.addAll(listInfoSeach);
        } else {
            for (InfoCard info : listInfoSeach) {
                if (info.getTittle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listInfo.add(info);
                }
            }
        }
        listApolo.setAdapter(new ListApoloAdapter(listInfo));
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        this.filter(s);
        return false;
    }
}