package com.softyou.eltiempo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.softyou.eltiempo.data.databaseRoom.InfoCard;
import com.softyou.eltiempo.viewmodel.LiveDataViewModel;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class DetailsApolo extends AppCompatActivity {

    @Inject
    LiveDataViewModel viewModel;

    private ImageView imageApollo;
    private TextView txtTittle;
    private Button btnfav;

    private int id;
    private boolean fav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_apolo);

        Intent intent = getIntent();
        id = intent.getIntExtra("id", -1);
        String tittle = intent.getStringExtra("tittle");
        String href = intent.getStringExtra("href");
        fav = intent.getBooleanExtra("fav", false);

        if(id != -1){
            viewModel.getItemDBApollo(id);
        }

        imageApollo = findViewById(R.id.imageApollo);
        txtTittle = findViewById(R.id.txtTittle);
        btnfav = findViewById(R.id.btnFav);

        txtTittle.setText(tittle);
        Picasso.with(imageApollo.getContext())
                    .load(href)
                    .into(imageApollo);
        if(fav){
            btnfav.setBackgroundResource(R.drawable.favorite2);
        }else{
            btnfav.setBackgroundResource(R.drawable.favorite);
        }

        btnfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isFav = !fav;
                if(isFav){
                    btnfav.setBackgroundResource(R.drawable.favorite2);
                }else{
                    btnfav.setBackgroundResource(R.drawable.favorite);
                }
                viewModel.updateItemDBApollo(DetailsApolo.this.id, isFav);
                fav = isFav;
            }
        });
    }
}