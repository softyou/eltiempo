package com.softyou.eltiempo.di;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class MainHiltApplication extends Application { }
