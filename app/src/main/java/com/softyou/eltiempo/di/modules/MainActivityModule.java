package com.softyou.eltiempo.di.modules;

import com.softyou.eltiempo.viewmodel.LiveDataViewModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;

@Module
@InstallIn(ActivityComponent.class)
public class MainActivityModule { }
