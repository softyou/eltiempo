package com.softyou.eltiempo.di.modules;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.softyou.eltiempo.data.ApoloInfoDBRepository;
import com.softyou.eltiempo.data.ApoloInfoRepository;
import com.softyou.eltiempo.data.IApiService;
import com.softyou.eltiempo.data.databaseRoom.ApoloDao;
import com.softyou.eltiempo.data.databaseRoom.DataBaseRoom;
import com.softyou.eltiempo.domain.ApoloUseCase;
import com.softyou.eltiempo.domain.IApoloRepository;
import com.softyou.eltiempo.repository.ApoloRepositoryImpl;
import com.softyou.eltiempo.viewmodel.LiveDataViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.qualifiers.ApplicationContext;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(ApplicationComponent.class)
public class AppModule {

    private static String urlbase= "https://images-api.nasa.gov/";

    @Provides
    public static OkHttpClient provideInterceptorOkHttpClient() {
        return new OkHttpClient.Builder()
                .build();
    }

    @Provides
    @Singleton
    public static IApiService retrofitProvider(OkHttpClient okHttpClient){

        return new Retrofit.Builder()
                 .baseUrl(urlbase)
                 .addConverterFactory(GsonConverterFactory.create())
                 .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                 .client(okHttpClient)
                 .build().create(IApiService.class);
    }

    @Provides
    public Context contextProvider(@ApplicationContext Context appContext ){
        return appContext;
    }

    @Provides
    @Singleton
    public DataBaseRoom dataBaseProvider(Context context)  {
        return Room.databaseBuilder(
                context,
                DataBaseRoom.class, "InfoCard"
        ).allowMainThreadQueries().build();
    }

    @Provides
    @Singleton
    public ApoloDao apoloProvider(DataBaseRoom dataBaseRoom)  {
        return dataBaseRoom.userDao();
    }

    @Provides
    @Singleton
    public static ApoloInfoRepository apoloInfoRepositoryProvider(IApiService iApiService){
        return new ApoloInfoRepository( iApiService);
    }

    @Provides
    @Singleton
    public static ApoloInfoDBRepository apoloInfoDBRepositoryProvider(ApoloDao apoloDao){
        return new ApoloInfoDBRepository(apoloDao);
    }

    @Provides
    @Singleton
    public static IApoloRepository liveIApoloRepositoryProvider(ApoloInfoRepository apoloInfoRepository,
                                                                ApoloInfoDBRepository apoloInfoDBRepository){
        return new ApoloRepositoryImpl(apoloInfoRepository, apoloInfoDBRepository);
    }

    @Provides
    @Singleton
    public static ApoloUseCase apoloUseCaseProvider(ApoloRepositoryImpl apoloRepositoryImpl){
        return new ApoloUseCase(apoloRepositoryImpl);
    }

    @Provides
    @Singleton
    public LiveDataViewModel liveDataViewModelProvider(ApoloUseCase apoloUseCase){
        return new LiveDataViewModel(apoloUseCase);
    }
}
